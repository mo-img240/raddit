$(document).ready(function () {
  $('body').tooltip({ selector: "[data-toggle~='tooltip']"});
  $('body').tooltip({ selector: '[rel="tooltip"]'});
});

// $('[rel=tooltip]').tooltip()          // Init tooltips
// $('[rel=tooltip]').tooltip('disable') // Disable tooltips
// $('[rel=tooltip]').tooltip('enable')  // (Re-)enable tooltips
// $('[rel=tooltip]').tooltip('destroy') // Hide and destroy tooltips
